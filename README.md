ADD ANDROID APP and CREATE WEBSITE
This web application provides a platform for admins to add Android apps and assign points to users who download them. It also allows users to view available apps, their earned points, complete tasks, and upload screenshots as proof of task completion.

Features

 Admin Facing:
 -Add Android apps with assigned points.
 -Manage app listings.

 User Facing:
 -Sign up and login.
 -View available apps and their points.
 -View personal profile, earned points, and completed tasks.
 -Upload screenshots for completed tasks.

Technologies Used

 Django: Python web framework.
 Django REST Framework: for creating RESTful APIs.
 MYSQL: Relational database for storing app and user data.
 CSRF Authentication: for user authentication.
 
Installation

 -Install the official python 3.6 or higher

 -git clone this repository

 -pip install virtualenv

 -create a virtualenv

 -run... pip install -r requirements.txt

 -create go to djangoapp

 -run... python manage.py runserver


Authentication
 -CSRF authentication is used for user authentication.
 -Users receive a CSRF token upon successful login, which they include in subsequent requests for authentication.