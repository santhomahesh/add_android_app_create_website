# views.py
from django.shortcuts import render,HttpResponse,redirect
from django.http import HttpResponse
from .models import AppAdmin
#import play_scraper
from django.contrib import messages

# Admin user and password
# It should be taken from json 
adminUser='sam'
adminPass='Sam&1256'



    
def adminPage(request):
    if request.method == 'POST':
        # Handling POST request
        name = request.POST.get('name')
        appID = request.POST.get('appID')
        iconLink = request.POST.get('iconLink')
        downloadLink = request.POST.get('downloadLink')
        category = request.POST.get('category')
        points = request.POST.get('points')

        # Create a new App object and save it to the database
        app = AppAdmin.objects.create(
            name=name,
            appID=appID,
            iconLink=iconLink,
            downloadLink=downloadLink,
            category=category,
            points=points
        )
        app.save()
        messages.success(request,'App added successfully!!')
        return redirect('admin-home')  # Redirect to a success page or wherever you want
    else:
        # Rendering the template for GET request
        return render(request, 'adminpage.html')




# If anyone searches for apps page then redirect him here
def apps(request):
    #If the user in session then only give him this page
    if request.session.has_key('username') and request.session['username']==adminUser:
        # Take all object and display it
        app_list=AppAdmin.objects.all()
        # Delete if a POST request comes
        if request.method=='POST':
            currentappID=request.POST['app']
            deleteObject=AppAdmin.objects.get(id=currentappID)
            deleteObject.delete()
        passToHtml={
            'title':'Added Apps',
            'app_list':app_list,            }
        return render(request,'apps.html',passToHtml)
    else:
        return redirect('admin-login')
        

        
        

# If login page is requested then it will come here
def adminLogin(request):
    # If he is already logged in don't ask him again 
    if request.session.has_key('username'):
        return redirect('admin/')
    else:
        if request.method=='POST':
            inputUser=request.POST['username']
            inputPass=request.POST['password']
            # If that is same as the admin user then redirect him to login page
            if inputUser==adminUser and inputPass==adminPass:
                request.session['username']=adminUser
                return redirect('admin/')
            # else send a warning flash-message that user Not found 
            else:
                messages.warning(request,'Not found')
        return render(request,'adminLogin.html')

# If log-out is requested then it will delete the session variable and redirects to login page
def logout(request):
    try:
        del request.session['username']
        return redirect('admin-login')
    except:
        return HttpResponse("<h1 style='margin:50px; font-size:80px;'><b>404 Not Found</b></h1>")